<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF 預覽</title>
</head>

<body>
    {{-- <div class="repair-order-wrapper p-3 w-50">
        <div class="repair-order-header w-100">
            <div class="company-info">
                <div class="company-name">NOVA553C 維修站</div>
                <div class="company-desc">手機·平板·筆電·桌機维修服務中心</div>
            </div>
        </div>
        <div class="repair-order-body w-100">
            <table class="table-bordered w-100">
                <tr>
                    <th>聯絡人</th>
                    <td>{{ isset($repair_order->name) ? $repair_order->name : '' }}</td>
                    <th>行動電話</th>
                    <td>{{ isset($repair_order->mobile) ? $repair_order->mobile : '' }}</td>
                    <th>室話</th>
                    <td colspan="2">{{ isset($repair_order->phone) ? $repair_order->phone : '' }}</td>
                </tr>
                <tr>
                    <th>公司名稱</th>
                    <td colspan="3">{{ isset($repair_order->company_name) ? $repair_order->company_name : '' }}</td>
                    <th>統一編號</th>
                    <td colspan="2">{{ isset($repair_order->tax_id_number) ? $repair_order->tax_id_number : '' }}</td>
                </tr>
                <tr>
                    <th>廠牌</th>
                    <td>{{ isset($repair_order->device_brand) ? $repair_order->device_brand : '' }}</td>
                    <th>機型</th>
                    <td>{{ isset($repair_order->device_model) ? $repair_order->device_model : '' }}</td>
                    <th>序號</th>
                    <td colspan="2">{{ isset($repair_order->device_serial_number) ? $repair_order->device_serial_number : '' }}</td>
                </tr>
                <tr>
                    <th>配件</th>
                    <td colspan="3">{{ isset($repair_order->device_accessories) ? $repair_order->device_accessories : '' }}</td>
                    <th>收件工程師</th>
                    <td colspan="2">{{ isset($repair_order->engineer) ? $repair_order->engineer : '' }}</td>
                </tr>
                <tr>
                    <th>外觀描述</th>
                    <td class="vat" colspan="3">{!! isset($repair_order->appearance_desc) ? nl2br($repair_order->appearance_desc) : '' !!}</td>
                    <td colspan="3" rowspan="3">
                        <table class="table-bordered w-100">
                            <tr>
                                <td colspan="7">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" {{ $repair_order->data_delete_check ? 'checked' : '' }} />
                                        <label class="form-check-label" for="">資料不須保留，確認可刪除</label>
                                    </div>


                                </td>
                            </tr>
                            <tr>
                                <th rowspan="2">

                                </th>
                                <th colspan="3">
                                    維修前
                                </th>
                                <th colspan="3">
                                    維修後
                                </th>
                            </tr>
                            <tr>
                                <td>是</td>
                                <td>否</td>
                                <td>無法檢測</td>
                                <td>是</td>
                                <td>否</td>
                                <td>無法檢測</td>
                            </tr>
                            <tr>
                                <td>按鈕</td>
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->before_check_btn == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->after_check_btn == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>螢幕</td>
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->before_check_monitor == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->after_check_monitor == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>網路</td>
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->before_check_internet == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->after_check_internet == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>通話</td>
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->before_check_call == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->after_check_call == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>鏡頭</td>
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->before_check_camera == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->after_check_camera == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>尾插</td>
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->before_check_bottom == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->after_check_bottom == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>喇叭</td>
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->before_check_speaker == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                                @foreach (['1', '0', '2'] as $value)
                                    <td>
                                        {{ $repair_order->after_check_speaker == $value ? 'v' : '' }}
                                    </td>
                                @endforeach
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>故障描述</th>
                    <td class="vat" colspan="3">{!! isset($repair_order->error_desc) ? nl2br($repair_order->error_desc) : '' !!}</td>
                </tr>
                <tr>
                    <th>處理報告</th>
                    <td class="vat" colspan="3">{!! isset($repair_order->repair_report) ? nl2br($repair_order->repair_report) : '' !!}</td>
                </tr>
                <tr>
                    <th>付款金額</th>
                    <td colspan="6">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-4">未稅總價</div>
                                <div class="col-4">稅金</div>
                                <div class="col-4">合計總價</div>
                            </div>
                            <div class="row">
                                <div class="col-4">已付訂金</div>
                                <div class="col-4">應收尾款</div>
                                <div class="col-4">檢測費用</div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>重要通知</th>
                    <td colspan="6">
                        <div>
                            <ul>
                                <li>1. 機器故障原因不明確，須留機器檢測問題後報價，報價不修收取換檢測拆機費。 ☐ 300 ☐ 500 ☐ 800</li>
                                <li>2. 完修及收費後屬同一間題之維修保固期為三個月，完修後經通知後七日未取回者，以通知日為保固算起日;維修保固站紙毁視同過保處理，尾插、按鈕、排線修補、泡水洗板及軟體不在保固範圍。</li>
                                <li>3. 維修品完修經電話通知後二週内末取回，本公司不負保管責任，逾一個月仍未取回及付清款時，本公司得以逕行處分該維修品，所得價款用以抵償修理費及保管費。</li>
                                <li>4. 凡因外力、泡水、人為破壞(自行拆修)或軟體所造成之不良，不屬保固範圍内。</li>
                                <li>5. 原機器外殼已破損，或老舊造成之脆化經拆修後無法還原至原狀，或破損範圍擴大，本公司不負任何因維修所造成責任。</li>
                                <li>6. 送修前機器故障原因不明確，經拆修無法還原至原送修狀況，或因敘述故障點造成之各種故障原因，本公司不負其相關責任。</li>
                                <li>7. 送修機器經拆機造成原廠認定不保之情形，本公司不負其相關責任。</li>
                                <li>8. 送修前機器無法開機或顯示時，致收件服務人員無法測機時，以檢測工程師測試為依據，不得有任何爭議。</li>
                                <li>9. 重要機密資料請自行備份，本公司概不負責。</li>
                                <li>10. 非合法之原版軟體本公司恕無法配合任何安裝教學等服務。</li>
                                <li>11. 如因維修料件無法取得，以致送修品無法修復，則按原件退還，不收取任何費用，本公司亦無須擔負完修責任。</li>
                                <li>12. 因應保密協定維修所更換之壞品零件恕不予以歸還。</li>
                                <li>13. 取件以取件聯為主，無取件聯時，得以相關證件證明取件。若因遺失取件聯而被冒領者，由客戶自行負責。</li>
                                <li>14. 如機後，料已下單時，恕須負擔維修費 50%。</li>
                                <li>15. 商品維修時如遇不可抗因素，導致維修品零件損壞，本店有權更換良品零件，資料部份恕無法還原。</li>
                            </ul>
                        </div>
                        <div>
                            客戶確認簽名:
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>取件記錄</th>
                    <th>配件齊全無誤取件人</th>
                    <td>簽名</td>
                    <th>取件日期</th>
                    <td>日期</td>
                    <th>收款業務</th>
                    <td>簽名</td>
                </tr>
            </table>
        </div>
        <div class="repair-order-footer w-100">
            <div class="contact-info d-flex">
                <div class="address">新竹 NOVA 店:新竹市東區光復路 2 段 194 巷 3 號 1 樓 NOVA 館內 143 櫃</div>
                <div class="tel">電話:(03)573-9898</div>
                <div class="mobile">手機:0966-333-798 143 櫃</div>
                <div class="email">信箱:nova553c@gmail.com</div>
                <div class="line">客服 LINE: @drw0183v</div>
                <div class="facebook">FB: 553c 維修站-新竹 Nova 館內</div>
                <div class="line-qrcode"></div>
            </div>
        </div>
    </div> --}}
    <div class="book" id="app">
        <div class="page">
            <div class="header">
                <div class="title">NOVA553C 維修站</div>
            </div>
            <div class="body">
                <table>
                    <tr>
                        <th>聯絡人</th>
                        <td>{{ isset($repair_order->name) ? $repair_order->name : '' }}</td>
                        <th>行動電話</th>
                        <td>{{ isset($repair_order->mobile) ? $repair_order->mobile : '' }}</td>
                        <th>室話</th>
                        <td colspan="2">{{ isset($repair_order->phone) ? $repair_order->phone : '' }}</td>
                    </tr>
                    <tr>
                        <th>公司名稱</th>
                        <td colspan="3">{{ isset($repair_order->company_name) ? $repair_order->company_name : '' }}</td>
                        <th>統一編號</th>
                        <td colspan="2">{{ isset($repair_order->tax_id_number) ? $repair_order->tax_id_number : '' }}</td>
                    </tr>
                    <tr>
                        <th>廠牌</th>
                        <td>{{ isset($repair_order->device_brand) ? $repair_order->device_brand : '' }}</td>
                        <th>機型</th>
                        <td>{{ isset($repair_order->device_model) ? $repair_order->device_model : '' }}</td>
                        <th>序號</th>
                        <td colspan="2">{{ isset($repair_order->device_serial_number) ? $repair_order->device_serial_number : '' }}</td>
                    </tr>
                    <tr>
                        <th>配件</th>
                        <td colspan="3">{{ isset($repair_order->device_accessories) ? $repair_order->device_accessories : '' }}</td>
                        <th>收件工程師</th>
                        <td colspan="2">{{ isset($repair_order->engineer) ? $repair_order->engineer : '' }}</td>
                    </tr>
                    <tr>
                        <th>外觀描述</th>
                        <td class="vat" colspan="3">{!! isset($repair_order->appearance_desc) ? nl2br($repair_order->appearance_desc) : '' !!}</td>
                        <td colspan="3" rowspan="3">
                            <table class="table-bordered w-100">
                                <tr>
                                    <td colspan="7">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" {{ $repair_order->data_delete_check ? 'checked' : '' }} />
                                            <label class="form-check-label" for="">資料不須保留，確認可刪除</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th rowspan="2">

                                    </th>
                                    <th colspan="3">
                                        維修前
                                    </th>
                                    <th colspan="3">
                                        維修後
                                    </th>
                                </tr>
                                <tr>
                                    <td>是</td>
                                    <td>否</td>
                                    <td>無法檢測</td>
                                    <td>是</td>
                                    <td>否</td>
                                    <td>無法檢測</td>
                                </tr>
                                <tr>
                                    <td>按鈕</td>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_btn == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_btn == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>螢幕</td>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_monitor == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_monitor == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>網路</td>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_internet == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_internet == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>通話</td>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_call == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_call == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>鏡頭</td>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_camera == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_camera == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>尾插</td>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_bottom == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_bottom == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td>喇叭</td>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_speaker == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_speaker == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>故障描述</th>
                        <td class="vat" colspan="3">{!! isset($repair_order->error_desc) ? nl2br($repair_order->error_desc) : '' !!}</td>
                    </tr>
                    <tr>
                        <th>處理報告</th>
                        <td class="vat" colspan="3">{!! isset($repair_order->repair_report) ? nl2br($repair_order->repair_report) : '' !!}</td>
                    </tr>
                    <tr>
                        <th>付款金額</th>
                        <td colspan="6">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-4">未稅總價</div>
                                    <div class="col-4">稅金</div>
                                    <div class="col-4">合計總價</div>
                                </div>
                                <div class="row">
                                    <div class="col-4">已付訂金</div>
                                    <div class="col-4">應收尾款</div>
                                    <div class="col-4">檢測費用</div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>重要通知</th>
                        <td colspan="6">
                            <div>
                                <ul>
                                    <li>1. 機器故障原因不明確，須留機器檢測問題後報價，報價不修收取換檢測拆機費。 ☐ 300 ☐ 500 ☐ 800</li>
                                    <li>2. 完修及收費後屬同一間題之維修保固期為三個月，完修後經通知後七日未取回者，以通知日為保固算起日;維修保固站紙毁視同過保處理，尾插、按鈕、排線修補、泡水洗板及軟體不在保固範圍。</li>
                                    <li>3. 維修品完修經電話通知後二週内末取回，本公司不負保管責任，逾一個月仍未取回及付清款時，本公司得以逕行處分該維修品，所得價款用以抵償修理費及保管費。</li>
                                    <li>4. 凡因外力、泡水、人為破壞(自行拆修)或軟體所造成之不良，不屬保固範圍内。</li>
                                    <li>5. 原機器外殼已破損，或老舊造成之脆化經拆修後無法還原至原狀，或破損範圍擴大，本公司不負任何因維修所造成責任。</li>
                                    <li>6. 送修前機器故障原因不明確，經拆修無法還原至原送修狀況，或因敘述故障點造成之各種故障原因，本公司不負其相關責任。</li>
                                    <li>7. 送修機器經拆機造成原廠認定不保之情形，本公司不負其相關責任。</li>
                                    <li>8. 送修前機器無法開機或顯示時，致收件服務人員無法測機時，以檢測工程師測試為依據，不得有任何爭議。</li>
                                    <li>9. 重要機密資料請自行備份，本公司概不負責。</li>
                                    <li>10. 非合法之原版軟體本公司恕無法配合任何安裝教學等服務。</li>
                                    <li>11. 如因維修料件無法取得，以致送修品無法修復，則按原件退還，不收取任何費用，本公司亦無須擔負完修責任。</li>
                                    <li>12. 因應保密協定維修所更換之壞品零件恕不予以歸還。</li>
                                    <li>13. 取件以取件聯為主，無取件聯時，得以相關證件證明取件。若因遺失取件聯而被冒領者，由客戶自行負責。</li>
                                    <li>14. 如機後，料已下單時，恕須負擔維修費 50%。</li>
                                    <li>15. 商品維修時如遇不可抗因素，導致維修品零件損壞，本店有權更換良品零件，資料部份恕無法還原。</li>
                                </ul>
                            </div>
                            <div>
                                客戶確認簽名:
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>取件記錄</th>
                        <th>配件齊全無誤取件人</th>
                        <td>簽名</td>
                        <th>取件日期</th>
                        <td>日期</td>
                        <th>收款業務</th>
                        <td>簽名</td>
                    </tr>
                </table>
            </div>
            <div class="header">
                <div class="info">聯絡方式</div>
            </div>
        </div>
    </div>
</body>

</html>

<style>
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font-size: 12px;
        font-family: 'notosanstc', sans-serif;
        box-sizing: border-box;
    }

    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    .page {
        width: 210mm;
        /*隨著紙張大小異動*/
        min-height: 297mm;
        height: 297mm;
        /*隨著紙張大小異動*/
        /* padding: 5mm 5mm; */
        /* margin: 10mm auto; */
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .page .header {

    }
    .page .body {

    }
    .page .body table {
        width: 100%;
        border-collapse: collapse;
    }
    .page .body table th,.page .body table td {

        border: 1px solid;
    }
    .page .footer {

    }

    .subpage {
        padding: 1cm;
        /* border: 5px red solid; */
        height: 257mm;
        /*隨著紙張大小異動*/
        outline: 2cm #FFEAEA solid;
    }

    @page {
        size: A4;
        margin: 0;
    }

    @media print {

        html,
        body {
            width: 210mm;
            /*隨著紙張大小異動*/
            height: 297mm;
            /*隨著紙張大小異動*/
        }

        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
