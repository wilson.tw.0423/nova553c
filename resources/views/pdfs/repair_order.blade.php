<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>維修單-{{ $repair_order->no }}</title>
</head>

<body>
    <div class="book" id="app">
        <div class="page">
            <a class="save-btn" href="#" onclick="window.print();">下載/列印</a>
            <div class="header">
                <div class="title">NOVA553C 維修站</div>
                <div class="sub-title">手機·平板·筆電·桌機维修服務中心</div>
                {{-- <ul class="header-info">
                    <li class="item">
                        <div>收件日期：{{ $repair_order->order_day }}</div>
                    </li>
                    <li class="item">
                        <div>訂單編號：{{ $repair_order->no }}</div>
                        <div>營業時間：11:00-21:30</div>
                    </li>
                </ul>
                <div class="line-qrcode">
                    <img src="{{ asset('images/line_qrcode.png') }}" alt="">
                </div>
                 --}}
                <ul class="header-info">
                    <li class="item">
                        <div>客戶服務紀錄單</div>
                        <div>營業時間：11:00-21:30</div>
                    </li>
                    <li class="item">
                        <div>收件日期：{{ $repair_order->order_day }}</div>
                        <div>訂單編號：{{ $repair_order->no }}</div>
                    </li>
                </ul>
            </div>
            <div class="body">
                <table>
                    <tr>
                        <th><span class="tb-title">聯絡人</span></th>
                        <td>{{ isset($repair_order->name) ? $repair_order->name : '' }}</td>
                        <th><span class="tb-title">行動電話</span></th>
                        <td>{{ isset($repair_order->mobile) ? $repair_order->mobile : '' }}</td>
                        <th><span class="tb-title">室話</span></th>
                        <td colspan="2">{{ isset($repair_order->phone) ? $repair_order->phone : '' }}</td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">公司名稱</span></th>
                        <td colspan="3">{{ isset($repair_order->company_name) ? $repair_order->company_name : '' }}</td>
                        <th><span class="tb-title">統一編號</span></th>
                        <td colspan="2">{{ isset($repair_order->tax_id_number) ? $repair_order->tax_id_number : '' }}</td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">廠牌</span></th>
                        <td>{{ isset($repair_order->device_brand) ? $repair_order->device_brand : '' }}</td>
                        <th><span class="tb-title">機型</span></th>
                        <td>{{ isset($repair_order->device_model) ? $repair_order->device_model : '' }}</td>
                        <th><span class="tb-title">序號</span></th>
                        <td colspan="2">{{ isset($repair_order->device_serial_number) ? $repair_order->device_serial_number : '' }}</td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">配件</span></th>
                        <td colspan="3">{{ isset($repair_order->device_accessories) ? $repair_order->device_accessories : '' }}</td>
                        <th><span class="tb-title">收件工程師</span></th>
                        <td colspan="2">{{ isset($repair_order->engineer) ? $repair_order->engineer : '' }}</td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">外觀描述</span></th>
                        <td class="vat" colspan="3">{!! isset($repair_order->appearance_desc) ? nl2br($repair_order->appearance_desc) : '' !!}</td>
                        <td class="check-block" colspan="3" rowspan="3">
                            <table id="id-check-block">
                                <tr>
                                    <td colspan="7" class="border-top-0 border-left-0 border-right-0">
                                        <div class="data-delete-check">
                                            <input class="form-check-input" type="checkbox" {{ $repair_order->data_delete_check ? 'checked' : '' }} onclick="return false;" />
                                            <div>資料不須保留，確認可刪除</div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="border-left-0" rowspan="2"></th>
                                    <th colspan="3">
                                        維修前
                                    </th>
                                    <th colspan="3" class="border-right-0">
                                        維修後
                                    </th>
                                </tr>
                                <tr class="title">
                                    <th>是</th>
                                    <th>否</th>
                                    <th>無法檢測</th>
                                    <th>是</th>
                                    <th>否</th>
                                    <th class="border-right-0">無法檢測</th>
                                </tr>
                                <tr class="check-point">
                                    <th>按鈕</th>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_btn == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_btn == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr class="check-point">
                                    <th>螢幕</th>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_monitor == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_monitor == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr class="check-point">
                                    <th>網路</th>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_internet == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_internet == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr class="check-point">
                                    <th>通話</th>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_call == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_call == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr class="check-point">
                                    <th>鏡頭</th>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_camera == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_camera == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr class="check-point">
                                    <th>尾插</th>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_bottom == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_bottom == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                <tr class="check-point">
                                    <th>喇叭</th>
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->before_check_speaker == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                    @foreach (['1', '0', '2'] as $value)
                                        <td>
                                            {{ $repair_order->after_check_speaker == $value ? 'v' : '' }}
                                        </td>
                                    @endforeach
                                </tr>
                                {{-- <tr> --}}
                                {{-- <th colspan="3"><span class="tb-title">密碼鎖</span></th> --}}
                                {{-- <td colspan="4" align="left">{{ isset($repair_order->device_pwd_lock) ? $repair_order->device_pwd_lock : 0 }}</td> --}}
                                {{-- </tr> --}}
                                <tr>
                                    <th class="border-bottom-0 border-left-0"><span class="tb-title">密碼鎖</span></th>
                                    <td class="border-bottom-0" colspan="2">{{ isset($repair_order->device_pwd_lock) ? $repair_order->device_pwd_lock : 0 }}</td>
                                    <th class="border-bottom-0"><span class="tb-title">圖形</span></th>
                                    <td class="border-bottom-0 border-right-0" colspan="3">
                                        <div class="pattern-lock-container">
                                            <div class="lock-grid">
                                                <div class="lock-node" data-node="0"></div>
                                                <div class="lock-node" data-node="1"></div>
                                                <div class="lock-node" data-node="2"></div>
                                                <div class="lock-node" data-node="3"></div>
                                                <div class="lock-node" data-node="4"></div>
                                                <div class="lock-node" data-node="5"></div>
                                                <div class="lock-node" data-node="6"></div>
                                                <div class="lock-node" data-node="7"></div>
                                                <div class="lock-node" data-node="8"></div>
                                            </div>
                                            <svg class="svg-container" width="100%" height="100%"></svg>
                                        </div>
                                        <input type="hidden" class="form-control" id="form_device_pattern_lock" name="device_pattern_lock" value="{{ isset($repair_order->device_pattern_lock) ? $repair_order->device_pattern_lock : '' }}">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">故障描述</span></th>
                        <td class="vat" colspan="3">{!! isset($repair_order->error_desc) ? nl2br($repair_order->error_desc) : '' !!}</td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">處理報告</span></th>
                        <td class="vat" colspan="3">{!! isset($repair_order->repair_report) ? nl2br($repair_order->repair_report) : '' !!}</td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">付款金額</span></th>
                        <td colspan="6">
                            <div class="order-price">
                                <ul class="items">
                                    <li class="item">
                                        <div class="title tb-title">未稅總價</div>
                                        <div>：</div>
                                        <div class="price">${{ isset($repair_order->payment_pre_tax_price) ? number_format($repair_order->payment_pre_tax_price) : 0 }}</div>
                                    </li>
                                    <li class="item">
                                        <div class="title tb-title">稅金</div>
                                        <div>：</div>
                                        <div class="price">${{ isset($repair_order->payment_tax_price) ? number_format($repair_order->payment_tax_price) : '' }}</div>
                                    </li>
                                    <li class="item">
                                        <div class="title tb-title">合計總價</div>
                                        <div>：</div>
                                        <div class="price">${{ isset($repair_order->payment_total_price) ? number_format($repair_order->payment_total_price) : '' }}</div>
                                    </li>
                                </ul>
                                <ul class="items">
                                    <li class="item">
                                        <div class="title tb-title">已付訂金</div>
                                        <div>：</div>
                                        <div class="price">${{ isset($repair_order->payment_downpayment) ? number_format($repair_order->payment_downpayment) : '' }}</div>
                                    </li>
                                    <li class="item">
                                        <div class="title tb-title">應收尾款</div>
                                        <div>：</div>
                                        <div class="price">${{ isset($repair_order->payment_finalpayment) ? number_format($repair_order->payment_finalpayment) : '' }}</div>
                                    </li>
                                    <li class="item">
                                        <div class="title tb-title">檢測費用</div>
                                        <div>：</div>
                                        <div class="price">${{ isset($repair_order->payment_testing_charges) ? number_format($repair_order->payment_testing_charges) : '' }}</div>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><span class="tb-title">重要通知</span></th>
                        <td colspan="6">
                            <div>
                                <ul class="notice">
                                    <li>機器故障原因不明確，須留機器檢測問題後報價，報價不修收取換檢測拆機費。 ☐ 300 ☐ 500 ☐ 800</li>
                                    <li>完修及收費後屬同一間題之維修保固期為三個月，完修後經通知後七日未取回者，以通知日為保固算起日;維修保固站紙毁視同過保處理，尾插、按鈕、排線修補、泡水洗板及軟體不在保固範圍。</li>
                                    <li>維修品完修經電話通知後二週内末取回，本公司不負保管責任，逾一個月仍未取回及付清款時，本公司得以逕行處分該維修品，所得價款用以抵償修理費及保管費。</li>
                                    <li>凡因外力、泡水、人為破壞(自行拆修)或軟體所造成之不良，不屬保固範圍内。</li>
                                    <li>原機器外殼已破損，或老舊造成之脆化經拆修後無法還原至原狀，或破損範圍擴大，本公司不負任何因維修所造成責任。</li>
                                    <li>送修前機器故障原因不明確，經拆修無法還原至原送修狀況，或因敘述故障點造成之各種故障原因，本公司不負其相關責任。</li>
                                    <li>送修機器經拆機造成原廠認定不保之情形，本公司不負其相關責任。</li>
                                    <li>送修前機器無法開機或顯示時，致收件服務人員無法測機時，以檢測工程師測試為依據，不得有任何爭議。</li>
                                    <li>重要機密資料請自行備份，本公司概不負責。</li>
                                    <li>非合法之原版軟體本公司恕無法配合任何安裝教學等服務。</li>
                                    <li>如因維修料件無法取得，以致送修品無法修復，則按原件退還，不收取任何費用，本公司亦無須擔負完修責任。</li>
                                    <li>因應保密協定維修所更換之壞品零件恕不予以歸還。</li>
                                    <li>取件以取件聯為主，無取件聯時，得以相關證件證明取件。若因遺失取件聯而被冒領者，由客戶自行負責。</li>
                                    <li>如機後，料已下單時，恕須負擔維修費 50%。</li>
                                    <li>商品維修時如遇不可抗因素，導致維修品零件損壞，本店有權更換良品零件，資料部份恕無法還原。</li>
                                </ul>
                            </div>
                            <div class="customer-sign">
                                <div class="title">客戶確認簽名：</div>
                                <div class="input"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th width="10%"><span class="tb-title">取件記錄</span></th>
                        <th width="15%"><span class="tb-title">配件無誤取件人</span></th>
                        <td width="15%">{{ isset($repair_order->payment_downpayment) ? number_format($repair_order->payment_downpayment) : '' }}</td>
                        <th width="15%"><span class="tb-title">取件日期</span></th>
                        <td width="15%">{{ isset($repair_order->payment_downpayment) ? number_format($repair_order->payment_downpayment) : '' }}</td>
                        <th width="15%"><span class="tb-title">收款業務</span></th>
                        <td width="15%">{{ $repair_order->sales }}</td>
                    </tr>
                </table>
            </div>
            <div class="footer">
                <div class="contact-info">
                    <div class="left">
                        <div class="info address">新竹NOVA店：新竹市東區光復路2段194巷3號1樓NOVA館內143櫃</div>
                        <div class="info tel">電話：(03)573-9898</div>
                        <div class="info mobile">手機：0966-333-798</div>
                        <div class="info email">信箱：nova553c@gmail.com</div>
                        <div class="info facebook">FB：553c維修站-新竹Nova142櫃</div>
                        <div class="info line">客服LINE：@drw0183v</div>
                    </div>
                    <div class="right">
                        <div class="line-qrcode">
                            <img src="{{ asset('images/line_qrcode.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            let pattern = [];
            let directions = [];
            let patternJson = $("#form_device_pattern_lock").val();

            if (patternJson !== undefined && patternJson !== '') {

                console.log('parse patternJson');

                pattern = JSON.parse(patternJson);

                if (pattern.length > 0) {

                    let lastNode = '';

                    pattern.forEach(function(value) {

                        let thisNode = $('.lock-node[data-node="' + value + '"]');

                        thisNode.addClass('active');

                        if (lastNode) {
                            drawArrow(lastNode, thisNode);
                        }
                        lastNode = thisNode;
                    });
                }
            }

            function getDirection(start, end) {
                const directions = {
                    '0,1': 'R',
                    '1,0': 'L',
                    '0,-1': 'L',
                    '-1,0': 'R',
                    '1,1': 'RD',
                    '-1,-1': 'LU',
                    '1,-1': 'RU',
                    '-1,1': 'LD'
                };
                const dx = end[0] - start[0];
                const dy = end[1] - start[1];
                return directions[`${dx},${dy}`] || '';
            }

            function getCoordinates(node) {
                const index = parseInt(node.data('node'));
                return [index % 3, Math.floor(index / 3)];
            }

            function drawArrow(startNode, endNode) {
                const start = startNode.position();
                const end = endNode.position();
                const startX = start.left + startNode.width() / 2;
                const startY = start.top + startNode.height() / 2;
                const endX = end.left + endNode.width() / 2;
                const endY = end.top + endNode.height() / 2;

                const svg = document.querySelector('svg');
                const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
                line.setAttribute('x1', startX);
                line.setAttribute('y1', startY);
                line.setAttribute('x2', endX);
                line.setAttribute('y2', endY);
                line.setAttribute('class', 'arrow');

                console.log('line', line);
                svg.appendChild(line);

            }
        });
    </script>
</body>

</html>

<style>
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font-size: 10pt;
        font-family: 'notosanstc', sans-serif;
        box-sizing: border-box;
    }

    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    ul,
    li {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .page {
        width: 210mm;
        /*隨著紙張大小異動*/
        min-height: 297mm;
        height: 297mm;
        /*隨著紙張大小異動*/
        padding: 2mm 3mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        position: relative;
    }

    .save-btn {
        position: absolute;
        font-size: 25px;
        right: -130px;
    }

    .page .header {
        width: 100%;
    }

    .page .header .title,
    .page .header .sub-title {
        text-align: center;
    }

    .page .header .title {
        font-weight: 900;
        font-size: 24pt;
    }

    .page .header .sub-title {
        font-size: 12pt;
    }

    .page .header .header-info .item {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
    }

    .page .header .header-info .item:nth-of-type(1) {
        margin-bottom: 2pt;
    }

    .page .body {
        widows: 100%;
    }

    .page .body table {
        width: 100%;
        border-collapse: collapse;
    }

    .page .body table th,
    .page .body table td {

        border: 1px solid #666;
        padding: 2pt 5pt;
    }

    .tb-title {
        display: inline-block;
        width: 100%;
        text-align: justify;
        text-align-last: justify;
        text-justify: distribute-all-lines;
    }

    .order-price .items {
        display: flex;
        flex-direction: row;
        margin: 5pt 0;
    }

    .order-price .items .item {
        display: flex;
        flex-direction: row;
        width: calc(100% / 3);
        align-items: flex-end;
    }

    .order-price .items .item .title {
        width: 25%;
    }

    .order-price .items .item .price {
        width: 65%;
        border-bottom: 1px solid #666;
        padding: 0 5pt
    }

    .page .body table td.check-block {
        padding: 0;
    }

    .vat {
        vertical-align: top;
    }

    #id-check-block {
        margin: 0;
        border: 0;
    }

    #id-check-block td {

        text-align: center;
    }

    #id-check-block .title th {

        /* text-align: center; */
        /* width: calc(100%/3); */
    }

    #id-check-block .check-point th {
        border-left: 0 !important;
    }

    #id-check-block .check-point td:nth-last-child(1) {
        border-right: 0 !important;
    }

    .data-delete-check {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .data-delete-check div {
        margin-left: 5pt;
    }

    .border-top-0 {
        border-top: 0 !important;
    }

    .border-left-0 {
        border-left: 0 !important;
    }

    .border-bottom-0 {
        border-bottom: 0 !important;
    }

    .border-right-0 {
        border-right: 0 !important;
    }

    ul.notice {
        padding-left: 1.5em;
        margin-bottom: 0.6em;
        font-size: 9pt;
    }

    .notice li {
        list-style: decimal;
        margin: unset;
        padding: unset;

    }

    .customer-sign {
        margin-bottom: 0.5em;
        display: flex;
        justify-content: flex-end;
    }

    .customer-sign .title {
        font-size: 1.1em;
    }

    .customer-sign .input {
        width: 15em;
        border-bottom: 1px solid #666;
    }

    .page .footer {
        margin-top: 2pt;
    }

    .contact-info {
        display: flex;
        justify-content: space-between;
    }

    .contact-info .left {
        width: 83%;
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
        flex-direction: row;
        align-content: flex-start;
    }

    .contact-info .left .info {
        margin-right: 5pt;
    }

    .contact-info .left .info {
        margin-bottom: 5pt;
    }

    .contact-info .left .address {
        width: 100%;
    }

    .contact-info .right {
        width: 13%;
    }

    .line-qrcode img {
        max-width: 100%;
    }

    .subpage {
        padding: 1cm;
        /* border: 5px red solid; */
        height: 257mm;
        /*隨著紙張大小異動*/
        outline: 2cm #FFEAEA solid;
    }

    /* 圖形鎖 */
    .lock-grid {
        display: grid;
        grid-template-columns: repeat(3, 20px);
        grid-template-rows: repeat(3, 20px);
        gap: 5px;
        width: fit-content;
        margin: 2pt auto;
    }

    .lock-node {
        width: 20px;
        height: 20px;
        background-color: #fff;
        border: 1px solid #000;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        position: relative;
    }

    .lock-node.active {
        background-color: #ddd;
    }

    .lock-node.active:after {
        content: '';
        width: 5px;
        height: 5px;
        background-color: #fff;
        border-radius: 50%;
        position: absolute;
    }

    .arrow {
        stroke: red;
        stroke-width: 2;
    }

    .pattern-lock-container {
        position: relative;
    }

    .svg-container {
        position: absolute;
        top: 0;
        left: 0;
        pointer-events: none;
    }



    @page {
        size: A4;
        margin: 0;
    }

    @media print {

        html,
        body {
            width: 210mm;
            /*隨著紙張大小異動*/
            height: 297mm;
            /*隨著紙張大小異動*/
            /* padding: 1%; */
        }

        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            /* page-break-after: always; */
        }

        .save-btn {
            display: none;
        }

        /* .arrow {
            background-color: red;
            height: 2px;
        } */
    }
</style>
