@extends('layouts.admin')

@section('title', 'Order')

@section('content')

    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary border-bottom p-y-2">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    {{-- <li class="nav-item">
                        <a class="btn btn-primary" href="#" role="button">儲存</a>
                    </li> --}}
                    <li class="nav-item">
                        <a href="{{ route('task.create') }}"><button type="button" class="btn btn-primary">新增</button></a>
                    </li>
                    <li class="nav-item">
                        <button type="button" class="btn btn-primary">匯出</button>
                    </li>
                </ul>
                <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
                    <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
                </form>
            </div>
        </div>
    </nav>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th scope="col">序號</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>
                    <a href="{{ route('task.show', 1) }}"><button type="button"
                            class="btn btn-outline-info">查看</button></a>
                    <a href="{{ route('task.edit', 1) }}"><button type="button"
                            class="btn btn-outline-primary">編輯</button></a>
                    <a href="{{ route('task.destroy', 1) }}"><button type="button"
                            class="btn btn-outline-danger">刪除</button></a>
                </td>
            </tr>
            {{-- <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Larry the Bird</td>
                <td>Larry the Bird</td>
                <td>@twitter</td>
            </tr> --}}
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>


@endsection
