@extends('layouts.admin')

@section('title', 'Task.Edit')

@section('content')

    <div>新增Task</div>
    <div><a href="{{ route('task.index') }}">返回</a></div>
    <form action="">
        <div class="conrainer">
            <div class="row g-3">
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form-customer" name="customer_name"
                            placeholder="placeholder">
                        <label for="form-customer">姓名</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form-mobile" name="mobile"
                            placeholder="placeholder">
                        <label for="form-mobile">行動電話</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form-phone" name="phone" placeholder="placeholder">
                        <label for="form-phone">室話</label>
                    </div>
                </div>
            </div>
            <div class="row g-3">
                <div class="col-8">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form-company" name="company_name"
                            placeholder="placeholder">
                        <label for="form-company">公司名稱</label>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form_gui_number" name="gui_number"
                            placeholder="placeholder">
                        <label for="form-form_gui_number">統一編號</label>
                    </div>
                </div>
            </div>
            <div class="row g-3">
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form_device_brand" name="device_brand"
                            placeholder="placeholder">
                        <label for="form_device_brand">廠牌</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form_device_model" name="device_model"
                            placeholder="placeholder">
                        <label for="form_device_model">型號</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form_engineer" name="engineer"
                            placeholder="placeholder">
                        <label for="form_engineer">收件工程師</label>
                    </div>
                </div>
            </div>
            <div class="row g-3">
                <div class="col">
                    <div class="form-floating mb-3">
                        <div class="form-control d-flex align-items-center" style="padding: 1rem 0.75rem;">
                            <div>手機清單</div>
                            <div class="d-flex align-items-center ms-3">
                                <div class="fd-flex align-items-center me-3">
                                    <input class="form-check-input" type="checkbox" id="form_sim_card" value="sim_card">
                                    <label class="form-check-label" for="form_sim_card">SIM卡</label>
                                </div>
                                <div class="fd-flex align-items-center me-3">
                                    <input class="form-check-input" type="checkbox" id="form_sd_card" value="sd_card">
                                    <label class="form-check-label" for="form_sd_card">SD卡</label>
                                </div>
                                <div class="fd-flex align-items-center me-3">
                                    <input class="form-check-input" type="checkbox" id="form_card_case" value="card_case">
                                    <label class="form-check-label" for="form_card_case">卡托</label>
                                </div>
                                <div class="fd-flex align-items-center me-3">
                                    <input class="form-check-input" type="checkbox" id="form_bettery" value="bettery">
                                    <label class="form-check-label" for="form_bettery">電池</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form_device_serial_number"
                            name="device_serial_number" placeholder="placeholder">
                        <label for="form_device_serial_number">其他清單</label>
                    </div>
                </div>
                <div class="col">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="form_device_serial_number"
                            name="device_serial_number" placeholder="placeholder">
                        <label for="form_device_serial_number">序號</label>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
