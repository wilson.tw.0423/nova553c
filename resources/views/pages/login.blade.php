@extends('layouts.login')

@section('title', 'Login')

@section('content')
    <div style="width: 450px;margin: 0 auto;margin-top: 15%;">
        <form action="/login" method="post">
            @csrf
            <h1 class="h3 mb-3 fw-normal text-center">NOVA553C 後台系統</h1>
            <div class="form-floating">
                <input type="text" class="form-control" id="floatingInput" name='username'>
                <label for="floatingInput">Username</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="floatingPassword" name='password'>
                <label for="floatingPassword">Password</label>
            </div>
            <div class="form-floating">
                <button class="btn btn-primary w-100 py-2" type="submit">登入</button>
            </div>
            <div class="err">
                {{ isset($err) ? $err : '' }}
            </div>
        </form>
    </div>
@endsection
