@extends('layouts.admin')

@section('title', '維修單')

@section('content')

    <div class="admin-navbar shadow-lg p-3 mb-5 bg-body rounded">
        <div class="d-flex position-relative">
            <ul class="d-flex">
                <li class="nav-item">
                    <a href="{{ route('repair_order.create') }}"><button type="button" class="btn btn-outline-primary">新增</button></a>
                </li>
                <li class="nav-item">
                    <button type="button" class="btn btn-outline-primary" onclick="showExportOption()">匯出</button>
                </li>
            </ul>
            <div class="position-absolute end-0">
                <form class="d-flex" action="{{ route('repair_order.index') }}" method="GET">
                    @csrf
                    <input type="search" class="form-control me-2" placeholder="手機查詢..." aria-label="Search" name="mobile" value="{{ isset($_GET['mobile']) ? $_GET['mobile'] : '' }}">
                    <button class="btn btn-outline-primary" type="submit">Search</button>
                </form>
            </div>
            <div id="swal-export-options" class="d-none">
                <form action="{{ route('export.repair_order') }}" method="POST">
                    @csrf
                    <div class="export-option-block w-100 text-start">
                        <div class="row mb-4 w-100 mx-auto g-0 justify-content-between">
                            <div class="col-5">
                                <label for="form_export_start_date" class="form-label">開始日期</label>
                                <input type="date" class="form-control" id="form_export_start_date" name="export_start_date">
                            </div>
                            <div class="col-5">
                                <label for="form_export_end_date" class="form-label">結束日期</label>
                                <input type="date" class="form-control" id="form_export_end_date" name="export_end_date">
                            </div>
                        </div>
                        <div class="row w-100 text-end g-0">
                            <div class="col">
                                <button type="submit" class="btn btn-primary" onclick="Swal.close()">執行</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="card w-100 mb-5 shadow-lg overflow-hidden">
        @if ($repair_orders->isEmpty())
            <div class="card-body w-100">
                <h5 class="card-title text-center">目前查無相關資料</h5>
            </div>
        @else
            <div class="card-body w-100 p-0">
                <table class="table table-striped  text-center m-0">
                    <thead class="table-secondary">
                        <tr class="text-center">
                            {{-- <th scope="col"><input class="form-check-input" type="checkbox" id="check-all" /></th> --}}
                            <th scope="col" width="5%">序號</th>
                            <th scope="col" width="10%">維修單號</th>
                            <th scope="col" width="6%">姓名</th>
                            <th scope="col" width="6%">手機</th>
                            <th scope="col" width="6%">市話</th>
                            <th scope="col" width="*">公司</th>
                            <th scope="col" width="6%">統編</th>
                            <th scope="col" width="10%">取件日期</th>
                            <th scope="col" width="10%">建檔日期</th>
                            <th scope="col" width="15%">操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($repair_orders as $index => $repair_order)
                            <tr>
                                {{-- <th class="text-center" scope="row"><input class="form-check-input" type="checkbox" name="item[]" value="{{ $index + 1 }}" /></th> --}}
                                <th class="text-center" scope="row">{{ $index + 1 }}</th>
                                <td class="text-center">{{ $repair_order->no }}</td>
                                <td>{{ $repair_order->name }}</td>
                                <td>{{ $repair_order->mobile }}</td>
                                <td>{{ $repair_order->phone }}</td>
                                <td>{{ $repair_order->company_name }}</td>
                                <td>{{ $repair_order->tax_id_number }}</td>
                                <td>{!! $repair_order->pickup_date ? $repair_order->pickup_date : "<button type='button' class='btn btn-sm btn-warning'>尚未取件</button>" !!}</td>
                                <td>{{ $repair_order->created_at }}</td>
                                <td class="d-flex justify-content-center">
                                    {{-- <div class="mx-2"><a href="{{ route('repair_order.show', $repair_order->sn) }}"><button type="button" class="btn btn-outline-info">查看</button></a></div> --}}
                                    <div class="mx-2"><a href="{{ route('preview-pdf', ['repair_order', $repair_order->sn]) }}" target="_blank"><button type="button" class="btn btn-outline-info">預覽</button></a></div>
                                    <div class="mx-2"><a href="{{ route('repair_order.edit', $repair_order->sn) }}"><button type="button" class="btn btn-outline-primary">編輯</button></a></div>
                                    <div class="mx-2">
                                        <form name="delete-form" action="{{ route('repair_order.destroy', $repair_order->sn) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-outline-danger" onclick="return formDeleteConfirm()">刪除</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    {{ $repair_orders->links() }}

    @vite(['resources/js/pages/repair_order/index.js'])
@endsection
