@extends('layouts.admin')

@section('title', '維修單 - 新增')

@section('content')

    <form id="create-form" action="{{ route('repair_order.store') }}" method="POST">
        <div class="admin-navbar-top shadow-lg p-3 mb-5 bg-body rounded">
            <div class="d-flex position-relative">
                <ul class="d-flex">
                    <li class="nav-item">
                        <a href="{{ route('repair_order.index') }}"><button type="button" class="btn btn-outline-primary">返回</button></a>
                    </li>
                    <li class="nav-item">
                        <button type="submit" class="btn btn-outline-primary">儲存</button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            @csrf
            {{-- 顧客資訊 --}}
            <div class="row form-block mb-3">
                <div class="col">
                    <div class="card text-dark bg-light">
                        <div class="card-header text-primary">顧客資訊</div>
                        <div class="card-body">
                            <div class="row g-1">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_name" class="form-label">聯絡人</label>
                                        <input type="text" class="form-control" id="form_name" name="name">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_mobile" class="form-label">行動電話</label>
                                        <input type="text" class="form-control" id="form_mobile" name="mobile">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_phone" class="form-label">市話</label>
                                        <input type="text" class="form-control" id="form_phone" name="phone">
                                    </div>
                                </div>
                            </div>
                            <div class="row g-1">
                                <div class="col-8">
                                    <div class="mb-3">
                                        <label for="form_company_name" class="form-label">公司名稱</label>
                                        <input type="text" class="form-control" id="form_company_name" name="company_name">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="form_tax_id_number" class="form-label">統編</label>
                                        <input type="text" class="form-control" id="form_tax_id_number" name="tax_id_number">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- 設備資訊 --}}
            <div class="row form-block mb-3">
                <div class="col">
                    <div class="card text-dark bg-light">
                        <div class="card-header text-primary">設備資訊</div>
                        <div class="card-body">
                            <div class="row g-1">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_type" class="form-label">設備類型</label>
                                        <select id="form_device_type" class="form-select" name="device_type">
                                            @if (!empty($param['device_type']))
                                                @foreach ($param['device_type'] as $device_type)
                                                    <option value="{{ $device_type }}">{{ $device_type }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_brand" class="form-label">廠牌</label>
                                        <select id="form_device_brand" class="form-select" name="device_brand">
                                            @if (!empty($param['device_brand']))
                                                @foreach ($param['device_brand'] as $device_brand)
                                                    <option value="{{ $device_brand }}">{{ $device_brand }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_model" class="form-label">型號</label>
                                        <select id="form_device_model" class="form-select" name="device_model">
                                            @if (!empty($param['device_model']))
                                                @foreach ($param['device_model'] as $device_model)
                                                    <option value="{{ $device_model }}">{{ $device_model }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_serial_number" class="form-label">序號</label>
                                        <input type="text" class="form-control" id="form_device_serial_number" name="device_serial_number">
                                    </div>
                                </div>
                            </div>
                            <div class="row g-1">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_cpu" class="form-label">CPU</label>
                                        <input type="text" class="form-control" id="form_device_cpu" name="device_cpu">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_ram" class="form-label">RAM</label>
                                        <input type="text" class="form-control" id="form_device_ram" name="device_ram">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_hdd" class="form-label">HDD</label>
                                        <input type="text" class="form-control" id="form_device_hdd" name="device_hdd">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_ssd" class="form-label">SSD</label>
                                        <input type="text" class="form-control" id="form_device_ssd" name="device_ssd">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_device_power" class="form-label">Power</label>
                                        <input type="text" class="form-control" id="form_device_power" name="device_power">
                                    </div>
                                </div>
                            </div>
                            <div class="row g-1">
                                <div class="col-9">
                                    <div class="mb-3">
                                        <label for="form_device_accessories" class="form-label">配件</label>
                                        <select id="form_device_accessories" class="form-select form-control" name="device_accessories[]" multiple="multiple">
                                            @if (!empty($param['device_accessory']))
                                                @foreach ($param['device_accessory'] as $device_accessory)
                                                    <option value="{{ $device_accessory }}">{{ $device_accessory }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="form_engineer" class="form-label">收件工程師</label>
                                        <input type="text" class="form-control" id="form_engineer" name="engineer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- 設備狀態 --}}
            <div class="row form-block mb-3">
                <div class="col">
                    <div class="card text-dark bg-light">
                        <div class="card-header text-primary">設備狀況</div>
                        <div class="card-body">
                            <div class="row g-3">
                                <div class="col">
                                    <div class="row g-1">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="form_appearance_desc" class="form-label">外觀描述</label>
                                                <textarea class="form-control" placeholder="請填寫描述..." id="form_appearance_desc" name="appearance_desc" style="height: 200px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-1">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="form_error_desc" class="form-label">故障描述</label>
                                                <textarea class="form-control" placeholder="請填寫描述..." id="form_error_desc" name="error_desc" style="height: 200px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-1">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="form_repair_report" class="form-label">處理報告</label>
                                                <textarea class="form-control" placeholder="請填寫描述..." id="form_repair_report" name="repair_report" style="height: 200px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row g-1">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="form_appearance_desc" class="form-label">檢測</label>
                                                <table class="table table-bordered text-center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="7" class="text-start">
                                                                <div class="form-check d-flex justify-content-end align-items-center">
                                                                    <input class="form-check-input float-none mt-0" type="checkbox" name="data_delete_check" value="1" id="form_data_delete_check">
                                                                    <label class="form-check-label ms-2" for="form_data_delete_check">
                                                                        資料無需保留，確認可刪除。
                                                                    </label>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th scope="col"></th>
                                                            <th scope="col" colspan="3">維修前</th>
                                                            <th scope="col" colspan="3">維修後</th>
                                                        </tr>
                                                        <tr>
                                                            <th scope="col"></th>
                                                            <th scope="col">是</th>
                                                            <th scope="col">否</th>
                                                            <th scope="col">無法檢測</th>
                                                            <th scope="col">是</th>
                                                            <th scope="col">否</th>
                                                            <th scope="col">無法檢測</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">按鈕</th>
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="before_check_btn" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="after_check_btn" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">螢幕</th>
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="before_check_monitor" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="after_check_monitor" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">網路</th>
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="before_check_internet" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="after_check_internet" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">通話</th>
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="before_check_call" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="after_check_call" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">鏡頭</th>
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="before_check_camera" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="after_check_camera" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">尾插</th>
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="before_check_bottom" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="after_check_bottom" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">喇叭</th>
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="before_check_speaker" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                            @foreach (['1', '0', '2'] as $value)
                                                                <td class="td-radio">
                                                                    <input class="form-check-input" type="radio" name="after_check_speaker" value="{{ $value }}" {{ $value == '1' ? 'checked' : '' }}>
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-1">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="form_device_pwd_lock" class="form-label">密碼鎖</label>
                                                <input type="text" class="form-control" id="form_device_pwd_lock" name="device_pwd_lock">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-1">
                                        <div class="d-flex">
                                            <div class="title me-5">
                                                <label for="form_device_pattern_lock" class="form-label">圖形鎖</label>
                                                <input type="hidden" class="form-control" id="form_device_pattern_lock" name="device_pattern_lock" value="">
                                            </div>
                                            <div class="lock-grid">
                                                <div class="lock-node" data-node="0"></div>
                                                <div class="lock-node" data-node="1"></div>
                                                <div class="lock-node" data-node="2"></div>
                                                <div class="lock-node" data-node="3"></div>
                                                <div class="lock-node" data-node="4"></div>
                                                <div class="lock-node" data-node="5"></div>
                                                <div class="lock-node" data-node="6"></div>
                                                <div class="lock-node" data-node="7"></div>
                                                <div class="lock-node" data-node="8"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- 付款資訊 --}}
            <div class="row form-block mb-3">
                <div class="col">
                    <div class="card text-dark bg-light">
                        <div class="card-header text-primary">付款資訊</div>
                        <div class="card-body">
                            <div class="row g-1">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_payment_pre_tax_price" class="form-label">未稅總價</label>
                                        <input type="number" class="form-control" id="form_payment_pre_tax_price" name="payment_pre_tax_price">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_payment_tax_price" class="form-label">稅金</label>
                                        <input type="number" class="form-control" id="form_payment_tax_price" name="payment_tax_price">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_payment_total_price" class="form-label">合計總價</label>
                                        <input type="number" class="form-control" id="form_payment_total_price" name="payment_total_price">
                                    </div>
                                </div>
                            </div>
                            <div class="row g-1">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_payment_downpayment" class="form-label">已付訂金</label>
                                        <input type="number" class="form-control" id="form_payment_downpayment" name="payment_downpayment">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_payment_finalpayment" class="form-label">應收尾款</label>
                                        <input type="number" class="form-control" id="form_payment_finalpayment" name="payment_finalpayment">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_payment_testing_charges" class="form-label">檢測費用</label>
                                        <input type="number" class="form-control" id="form_payment_testing_charges" name="payment_testing_charges">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- 其他資訊 --}}
            <div class="row form-block mb-3">
                <div class="col">
                    <div class="card text-dark bg-light">
                        <div class="card-header text-primary">其他資訊</div>
                        <div class="card-body">
                            <div class="row g-1">
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_notify_method" class="form-label">通知方式</label>
                                        <input type="text" class="form-control" id="form_notify_method" name="notify_method">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_notify_date" class="form-label">通知日期</label>
                                        <input type="date" class="form-control" id="form_notify_date" name="notify_date">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_notify_date" class="form-label">取件日期</label>
                                        <input type="date" class="form-control" id="form_notify_date" name="notify_date">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                        <label for="form_sales" class="form-label">收款業務</label>
                                        <input type="text" class="form-control" id="form_sales" name="sales">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-outline-primary">儲存</button>
        </div>
        <div class="admin-navbar-bottom shadow-lg p-3 mt-5 bg-body rounded">
            <div class="d-flex position-relative">
                <ul class="d-flex">
                    <li class="nav-item">
                        <a href="{{ route('repair_order.index') }}"><button type="button" class="btn btn-outline-primary">返回</button></a>
                    </li>
                    <li class="nav-item">
                        <button type="submit" class="btn btn-outline-primary">儲存</button>
                    </li>
                </ul>
            </div>
        </div>
    </form>

    @vite(['resources/js/pages/repair_order/create.js'])
@endsection
