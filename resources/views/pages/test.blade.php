{{-- @extends('layouts.app')

@section('title', 'TEST')

@section('main')

    
@endsection --}}

@php
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pattern Lock</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="lock-grid">
        <div class="lock-node" data-node="0"></div>
        <div class="lock-node" data-node="1"></div>
        <div class="lock-node" data-node="2"></div>
        <div class="lock-node" data-node="3"></div>
        <div class="lock-node" data-node="4"></div>
        <div class="lock-node" data-node="5"></div>
        <div class="lock-node" data-node="6"></div>
        <div class="lock-node" data-node="7"></div>
        <div class="lock-node" data-node="8"></div>
    </div>
    <button id="submitPattern">Submit Pattern</button>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    {{-- <script src="script.js"></script> --}}
    <script>
        $(document).ready(function() {
            let pattern = [];
            let directions = [];

            function getDirection(start, end) {
                const directions = {
                    '0,1': 'R',
                    '1,0': 'L',
                    '0,-1': 'L',
                    '-1,0': 'R',
                    '1,1': 'RD',
                    '-1,-1': 'LU',
                    '1,-1': 'RU',
                    '-1,1': 'LD'
                };
                const dx = end[0] - start[0];
                const dy = end[1] - start[1];
                return directions[`${dx},${dy}`] || '';
            }

            function getCoordinates(node) {
                const index = parseInt(node.data('node'));
                return [index % 3, Math.floor(index / 3)];
            }

            function drawArrow(startNode, endNode) {
                const start = startNode.position();
                const end = endNode.position();
                const startX = start.left + startNode.width() / 2;
                const startY = start.top + startNode.height() / 2;
                const endX = end.left + endNode.width() / 2;
                const endY = end.top + endNode.height() / 2;

                const arrow = $('<div class="arrow"></div>');
                $('body').append(arrow);

                arrow.css({
                    position: 'absolute',
                    top: startY,
                    left: startX,
                    width: Math.hypot(endX - startX, endY - startY),
                    height: '2px',
                    backgroundColor: 'red',
                    transformOrigin: '0 0',
                    transform: `rotate(${Math.atan2(endY - startY, endX - startX) * 180 / Math.PI}deg)`
                });
            }

            $('.lock-node').on('mousedown', function() {
                pattern = [];
                directions = [];
                $('.lock-node').removeClass('active');
                $('.arrow').remove();
                $(this).addClass('active');
                pattern.push($(this).data('node'));
            });

            $('.lock-node').on('mouseenter', function() {
                if (pattern.length > 0 && !pattern.includes($(this).data('node'))) {
                    $(this).addClass('active');
                    const lastNode = $('.lock-node[data-node="' + pattern[pattern.length - 1] + '"]');
                    const direction = getDirection(getCoordinates(lastNode), getCoordinates($(this)));
                    directions.push(direction);
                    drawArrow(lastNode, $(this));
                    pattern.push($(this).data('node'));
                }
            });

            $(document).on('mouseup', function() {
                if (pattern.length > 0) {
                    console.log('Pattern:', pattern);
                    console.log('Directions:', directions);
                    // 发送模式和方向到服务器
                    // $.ajax({
                    //     url: 'save_pattern.php',
                    //     method: 'POST',
                    //     data: {
                    //         pattern: pattern,
                    //         directions: directions
                    //     },
                    //     success: function(response) {
                    //         alert('Pattern saved: ' + response);
                    //     }
                    // });
                }
                pattern = [];
                directions = [];
            });

            $('#submitPattern').on('click', function() {
                if (pattern.length > 0) {
                    console.log('Pattern:', pattern);
                    console.log('Directions:', directions);
                    // 发送模式和方向到服务器
                    $.ajax({
                        url: 'save_pattern.php',
                        method: 'POST',
                        data: {
                            pattern: pattern,
                            directions: directions
                        },
                        success: function(response) {
                            alert('Pattern saved: ' + response);
                        }
                    });
                }
            });
        });
    </script>
</body>

</html>

<style>
    body {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        margin: 0;
        background-color: #f0f0f0;
    }

    .lock-grid {
        display: grid;
        grid-template-columns: repeat(3, 100px);
        grid-template-rows: repeat(3, 100px);
        gap: 10px;
    }

    .lock-node {
        width: 100px;
        height: 100px;
        background-color: #fff;
        border: 2px solid #000;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        position: relative;
    }

    .lock-node.active {
        background-color: #000;
    }

    .lock-node.active:after {
        content: '';
        width: 20px;
        height: 20px;
        background-color: #fff;
        border-radius: 50%;
        position: absolute;
    }
</style>
