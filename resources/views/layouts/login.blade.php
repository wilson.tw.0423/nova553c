@extends('layouts.app')

@section('title', 'Login')

@section('main')

    @yield('content')

@endsection
