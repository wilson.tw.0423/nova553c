@extends('layouts.app')

@section('main')
    @php
        $dashboardActive = Route::currentRouteName() == 'dashboard';
        $repairOrderActive = Route::currentRouteName() == 'repair_order.index';
    @endphp
    <div class="main container-fluid p-0">
        <nav id="admin-nav-bar" class="navbar navbar-expand-lg navbar-dark bg-dark border-bottom p-2 fixed-top shadow shadow-lg">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('repair_order.index') }}">NOVA553C</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('repair_order.index') }}">維修單管理</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                帳號
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="{{ route('logout') }}">登出</a></li>
                                {{-- <li><a class="dropdown-item" href="#">Another action</a></li> --}}
                                {{-- <li>
                                    <hr class="dropdown-divider">
                                </li> --}}
                                {{-- <li><a class="dropdown-item" href="#">Something else here</a></li> --}}
                            </ul>
                        </li>
                        {{-- <li class="nav-item">
                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                        </li> --}}
                    </ul>
                    <div class="dropdown text-end d-none">
                        <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="https://github.com/mdo.png" alt="mdo" width="32" height="32" class="rounded-circle">
                            <i class="fa-solid fa-circle-user"></i>
                        </a>
                        <ul class="admin-user-menu dropdown-menu text-small" aria-labelledby="dropdownUser1">
                            <li><a class="dropdown-item" href="#">New project...</a></li>
                            <li><a class="dropdown-item" href="#">Settings</a></li>
                            <li><a class="dropdown-item" href="#">Profile</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="{{ route('logout') }}">Sign out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="admin-content">
            @yield('content')
        </div>
    </div>
@endsection
