<table>
    <thead>
        <tr>
            @foreach ($fields as $field_name)
                <th style="text-align: center; background-color: #666666; color: #ffffff; font-size: 14px;">{{ isset($field_names[$field_name]) ? $field_names[$field_name] : $field_name }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($repair_orders as $index => $repair_order)
            <tr>
                {{-- <td style="background-color: {{ ($index % 2 == 0) ? '#dddddd' : '#ffffff' }}; font-size: 14px;">{{ $index + 1 }}</td> --}}
                @foreach ($fields as $field_name)
                    <td style="background-color: {{ ($index % 2 == 0) ? '#dddddd' : '#ffffff' }}; font-size: 14px;">{{ $repair_order->{$field_name} }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
