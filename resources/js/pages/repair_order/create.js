// import { flowRight } from "lodash";

$(function () {

    // 
    $("#form_device_type, #form_device_brand, #form_device_model, #form_device_accessories").select2({
        tags: true
    });

    //
    $(".td-radio").on('click', function () {

        $(this).children("input[type='radio']").prop('checked', true);

        console.log($(this).children("input[type='radio']"));
    })

    // 稅金自動計算
    $("#form_payment_pre_tax_price").on('change', function () {

        $("#form_payment_tax_price").attr('placeholder', $(this).val() * 0.05);
    })

    // 稅金自動計算
    $("#form_payment_pre_tax_price, #form_payment_tax_price").on('change', function () {

        let pre_tax = parseInt($("#form_payment_pre_tax_price").val()) || 0;

        let tax = parseInt($("#form_payment_tax_price").val()) || 0;

        $("#form_payment_total_price").val(pre_tax + tax);
    })

    // 訂金/尾款計算
    $("#form_payment_downpayment").on('change', function () {

        let total_price = parseInt($("#form_payment_total_price").val()) || 0;

        let downpayment = parseInt($(this).val()) || 0;

        $("#form_payment_finalpayment").val(total_price - downpayment);
    })

});