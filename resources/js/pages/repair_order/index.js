
function deleteConfirm(deleteUrl) {

    if (confirm('確定要刪除這筆資料嗎？')) {
        window.location.href = deleteUrl;
    }
}

function formDeleteConfirm(deleteUrl) {

    if (confirm('確定要刪除這筆資料嗎？')) {
        return true;
    }

    return false;
}

function showExportOption() {

    let export_option = $("#swal-export-options").html();

    Swal.fire({
        html: export_option,
        showConfirmButton: false,
        inputAutoFocus: false
    });
}

$(function () {

    // 全選按鈕
    $("#check-all").on('click', function () {

        $("input[name='item[]']").prop("checked", $(this).prop('checked'));
    });

    // 全選按鈕判斷
    $("input[name='item[]']").on('click', function () {

        let checked = true;

        $("input[name='item[]").each(function () {

            if ($(this).prop('checked') === false) {

                checked = false;

                return false;
            }
        })

        $("#check-all").prop('checked', checked);
    });

    // 刪除按鈕
    $("#delete-form").on('click', function () {

        if (confirm('確定要刪除這筆資料嗎？')) {

            $(this)['0'].submit();

        }
    });
})

window.deleteConfirm = deleteConfirm;
window.formDeleteConfirm = formDeleteConfirm;
window.showExportOption = showExportOption;