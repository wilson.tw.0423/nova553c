import jQuery from 'jquery';

window.$ = jQuery;

import './bootstrap';

import select2 from 'select2';

select2();

import Swal from 'sweetalert2'

window.Swal = Swal;

import './pattern_lock';

$(document).ready(function () {
    
    console.log('app.js document ready');
});
