import $ from 'jquery';

$(document).ready(function () {
    let pattern = [];
    let directions = [];
    let patternJson = $("#form_device_pattern_lock").val();

    if (patternJson !== undefined && patternJson !== '') {

        console.log('parse patternJson');

        pattern = JSON.parse(patternJson);

        if (pattern.length > 0) {

            let lastNode = '';

            pattern.forEach(function (value) {

                let thisNode = $('.lock-node[data-node="' + value + '"]');

                thisNode.addClass('active');

                if (lastNode) {
                    drawArrow(lastNode, thisNode);
                }
                lastNode = thisNode;
            });
        }
    }

    function getDirection(start, end) {
        const directions = {
            '0,1': 'R',
            '1,0': 'L',
            '0,-1': 'L',
            '-1,0': 'R',
            '1,1': 'RD',
            '-1,-1': 'LU',
            '1,-1': 'RU',
            '-1,1': 'LD'
        };
        const dx = end[0] - start[0];
        const dy = end[1] - start[1];
        return directions[`${dx},${dy}`] || '';
    }

    function getCoordinates(node) {
        const index = parseInt(node.data('node'));
        return [index % 3, Math.floor(index / 3)];
    }

    function drawArrow(startNode, endNode) {
        const start = startNode.position();
        const end = endNode.position();
        const startX = start.left + startNode.width() / 2;
        const startY = start.top + startNode.height() / 2;
        const endX = end.left + endNode.width() / 2;
        const endY = end.top + endNode.height() / 2;

        const arrow = $('<div class="arrow"></div>');
        $('.lock-grid').append(arrow);

        arrow.css({
            position: 'absolute',
            top: startY,
            left: startX,
            width: Math.hypot(endX - startX, endY - startY),
            height: '2px',
            backgroundColor: 'red',
            transformOrigin: '0 0',
            transform: `rotate(${Math.atan2(endY - startY, endX - startX) * 180 / Math.PI}deg)`
        });
    }

    $('.lock-node').on('mousedown', function () {
        pattern = [];
        directions = [];
        $('.lock-node').removeClass('active');
        $('.arrow').remove();
        $(this).addClass('active');
        pattern.push($(this).data('node'));
    });

    $('.lock-node').on('mouseenter', function () {
        if (pattern.length > 0 && !pattern.includes($(this).data('node'))) {
            $(this).addClass('active');
            const lastNode = $('.lock-node[data-node="' + pattern[pattern.length - 1] + '"]');
            const direction = getDirection(getCoordinates(lastNode), getCoordinates($(this)));
            directions.push(direction);
            drawArrow(lastNode, $(this));
            pattern.push($(this).data('node'));
        }
    });

    $(document).on('mouseup', function () {
        if (pattern.length > 0) {

            let pattern_json = JSON.stringify(pattern);

            console.log('Pattern:', pattern);
            console.log('Pattern_json:', pattern_json);
            console.log('Directions:', directions);

            $("#form_device_pattern_lock").val(pattern_json);
        }
        pattern = [];
        directions = [];
    });

    $('#submitPattern').on('click', function () {
        if (pattern.length > 0) {
            console.log('Pattern:', pattern);
            console.log('Directions:', directions);
            // 发送模式和方向到服务器
            $.ajax({
                url: 'save_pattern.php',
                method: 'POST',
                data: {
                    pattern: pattern,
                    directions: directions
                },
                success: function (response) {
                    alert('Pattern saved: ' + response);
                }
            });
        }
    });
});