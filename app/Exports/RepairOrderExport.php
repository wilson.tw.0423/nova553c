<?php

namespace App\Exports;

use App\Models\RepairOrder;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Schema;


class RepairOrderExport implements FromView
{
    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $fields = array_intersect(Schema::getColumnListing((new RepairOrder)->getTable()), RepairOrder::getExportFields());

        $repair_orders = RepairOrder::withAccessors(); // withAccessors 啟動字段替換

        if ($this->request->export_start_date != '' && $this->request->export_end_date != '') {

            $repair_orders->whereBetween('order_day', [$this->request->export_start_date, $this->request->export_end_date]);

        } else if ($this->request->export_start_date != '') {

            $repair_orders->where('order_day', '>=' ,$this->request->export_start_date);

        } else if ($this->request->export_end_date != '') {

            $repair_orders->where('order_day', '<=' ,$this->request->export_end_date);
        }
        
        $repair_orders = $repair_orders->get();

        $fields = array_intersect(Schema::getColumnListing((new RepairOrder)->getTable()), RepairOrder::getExportFields());

        $field_names = RepairOrder::getFieldNames();

        return view('excels.repair_order', compact('repair_orders', 'fields', 'field_names'));
    }
}
