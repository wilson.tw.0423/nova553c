<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class RepairOrder extends Model
{
    use HasFactory;
    use SoftDeletes;

    // protected $primaryKey = 'sn'; // 已在 Model 設定

    // protected $fillable = [
    //     'no',
    //     'name',
    //     'mobile',
    //     'phone',
    //     'company_name',
    //     'tax_id_number',
    //     'tax_id_number',
    //     'order_day',
    //     'modify_day'
    // ];
    protected $guarded = [];
    protected static $applyAccessors = false;

    public $check_names = ['0' => '否', '1' => '是', '2' => '無法檢測'];


    public static function getFieldNames()
    {
        return [
            'no' => '維修單號',
            'name' => '姓名',
            'mobile' => '手機號碼',
            'phone' => '市話',
            'company_name' => '公司名稱',
            'tax_id_number' => '統編',
            'engineer' => '收單工程師',
            'device_type' => '設備類型',
            'device_brand' => '廠牌',
            'device_model' => '型號',
            'device_serial_number' => '序號',
            'device_cpu' => 'CPU',
            'device_ram' => 'RAM',
            'device_hdd' => 'HDD',
            'device_ssd' => 'SSD',
            'device_power' => 'POWER',
            'device_accessories' => '配件',
            'device_pwd_lock' => '密碼鎖',
            'device_pattern_lock' => '圖形鎖',
            'appearance_desc' => '外觀描述',
            'error_desc' => '故障描述',
            'repair_report' => '處理報告',
            'before_check_btn' => '維修前按鈕',
            'after_check_btn' => '維修後按鈕',
            'before_check_monitor' => '維修前網路',
            'after_check_monitor' => '維修後螢幕',
            'before_check_internet' => '維修前網路',
            'after_check_internet' => '維修後網路',
            'before_check_call' => '維修前通話',
            'after_check_call' => '維修後通話',
            'before_check_camera' => '維修前鏡頭',
            'after_check_camera' => '維修後鏡頭',
            'before_check_bottom' => '維修前尾插',
            'after_check_bottom' => '維修後尾插',
            'before_check_speaker' => '維修前喇叭',
            'after_check_speaker' => '維修後喇叭',
            'data_delete_check' => '可刪除資料',
            'payment_pre_tax_price' => '未稅總價',
            'payment_tax_price' => '稅金',
            'payment_total_price' => '合計總價',
            'payment_downpayment' => '已付訂金',
            'payment_finalpayment' => '應收尾款',
            'payment_testing_charges' => '檢測費用',
            'notify_method' => '通知方式',
            'notify_date' => '通知日期',
            'pickup_date' => '取件日期',
            'sales' => '收款業務',
            'order_day' => '收件日期'
        ];
    }

    public static function getExportFields()
    {
        return [
            'no',
            'name',
            'mobile',
            'phone',
            'company_name',
            'tax_id_number',
            'engineer',
            'device_type',
            'device_brand',
            'device_model',
            'device_serial_number',
            'device_cpu',
            'device_ram',
            'device_hdd',
            'device_ssd',
            'device_power',
            'device_accessories',
            'device_pwd_lock',
            'device_pattern_lock',
            'appearance_desc',
            'error_desc',
            'repair_report',
            'before_check_btn',
            'after_check_btn',
            'before_check_monitor',
            'after_check_monitor',
            'before_check_internet',
            'after_check_internet',
            'before_check_call',
            'after_check_call',
            'before_check_camera',
            'after_check_camera',
            'before_check_bottom',
            'after_check_bottom',
            'before_check_speaker',
            'after_check_speaker',
            'data_delete_check',
            'payment_pre_tax_price',
            'payment_tax_price',
            'payment_total_price',
            'payment_downpayment',
            'payment_finalpayment',
            'payment_testing_charges',
            'notify_method',
            'notify_date',
            'pickup_date',
            'sales',
            'order_day'
        ];
    }

    protected function replaceCheckValue($value)
    {
        return $this->check_names[$value];
    }

    public function __get($key)
    {
        // 如果访问器未启用，直接返回原始属性值
        if (!static::$applyAccessors) {
            return parent::__get($key);
        }

        // 检查是否有需要替换逻辑的字段
        $fields_to_replace = [
            'before_check_btn',
            'after_check_btn',
            'before_check_monitor',
            'after_check_monitor',
            'before_check_internet',
            'after_check_internet',
            'before_check_call',
            'after_check_call',
            'before_check_camera',
            'after_check_camera',
            'before_check_bottom',
            'after_check_bottom',
            'before_check_speaker',
            'after_check_speaker',
            'data_delete_check'
        ];

        if (in_array($key, $fields_to_replace)) {
            return $this->replaceCheckValue(parent::__get($key));
        }

        return parent::__get($key);
    }

    // 方法来禁用访问器
    public static function enableAccessors()
    {
        static::$applyAccessors = true;
    }

    // 局部作用域来启用访问器
    public function scopeWithAccessors(Builder $query)
    {
        static::enableAccessors();
        return $query;
    }

}
