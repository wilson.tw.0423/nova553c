<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RepairOrder;
use Barryvdh\DomPDF\Facade\Pdf;
use Dompdf\Dompdf;


class PdfController extends Controller
{
    //

    public function downloadPdf($name, $sn)
    {
        if ($name == 'repair_order') {

            $repair_order = RepairOrder::where('sn', $sn)->first();
            $file_name = $name . '_' . $sn . '.pdf';
        }

        $pdf = Pdf::loadView('pdfs.repair_order', compact('repair_order'));

        return $pdf->download($file_name);
    }

    public function previewPdf($name, $sn)
    {
        if ($name == 'repair_order') {

            $repair_order = RepairOrder::where('sn', $sn)->first();
        }

        return view("pdfs.repair_order", compact('repair_order'));
    }
}
