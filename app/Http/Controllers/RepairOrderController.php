<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RepairOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Parameter;
use App\Exports\RepairOrderExport;
use Maatwebsite\Excel\Facades\Excel;

class RepairOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // DB::enableQueryLog();

        // 创建查询构建器
        $repair_orders_query = RepairOrder::query();

        // 定义查询条件
        $conditions = [
            'mobile' => $request->mobile,
            'name' => $request->name,
            // 可以继续添加其他查询条件
        ];

        // 根据条件动态添加查询
        foreach ($conditions as $field => $value) {
            $repair_orders_query->when($value, function ($query, $value) use ($field) {
                return $query->where($field, $value);
            });
        }

        // 添加排序
        $repair_orders_query->orderByDesc('sn');

        // 执行查询并进行分页
        $repair_orders = $repair_orders_query->paginate(10);

        // 返回视图并传递分页结果
        return response(view('pages.repair_order.index', compact('repair_orders')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $param = $this->getParameter(['device_type', 'device_brand', 'device_model', 'device_accessory']);

        return response(view('pages.repair_order.create', compact('param')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $repair_order_no = $this->generateOrderNo();

        $request_data = $request->only(Schema::getColumnListing((new RepairOrder)->getTable()));

        if (empty($request_data['data_delete_check']))
            $request_data['data_delete_check'] = '0';

        if (!empty($request_data['device_accessories']))
            $request_data['device_accessories'] = implode(',', $request_data['device_accessories']);

        $request_data['no'] = $repair_order_no;
        $request_data['order_day'] = thisDay();
        $request_data['modify_day'] = thisDay();

        RepairOrder::create($request_data);

        if ($request->device_type) {

            $this->checkParameter('device_type', $request->device_type);
        }

        if ($request->device_brand) {

            $this->checkParameter('device_brand', $request->device_brand);
        }

        if ($request->device_model) {

            $this->checkParameter('device_model', $request->device_model);
        }

        if ($request->device_accessories) {

            foreach ($request->device_accessories as $device_accessory) {

                $this->checkParameter('device_accessory', $device_accessory);
            }
        }

        return redirect()->route('repair_order.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $sn
     * @return \Illuminate\Http\Response
     */
    public function show($sn)
    {
        // Log::info('Showing the user profile for user: ', ['sn' => $sn]);

        // Log::info('User failed to login.', ['sn' => $sn]);

        $repair_order = RepairOrder::where('sn', $sn)->first();

        return response(view('pages.repair_order.show', compact('repair_order')));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $sn
     * @return \Illuminate\Http\Response
     */
    public function edit($sn)
    {
        // DB::enableQueryLog();

        $repair_order = RepairOrder::where('sn', $sn)->first();

        $repair_order->device_accessories = explode(',', $repair_order->device_accessories);

        // $queries = DB::getQueryLog();

        // dd($queries);

        $param = $this->getParameter(['device_type', 'device_brand', 'device_model', 'device_accessory']);

        if ($repair_order) {

            return response(view('pages.repair_order.edit', compact('repair_order', 'param')));

        } else {

            return response(view('pages.repair_order.edit', compact('repair_order', 'param')));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $sn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sn)
    {
        // dd($request->all());
        $request_data = $request->only(Schema::getColumnListing((new RepairOrder)->getTable()));

        if (empty($request_data['data_delete_check']))
            $request_data['data_delete_check'] = '0';

        if (!empty($request_data['device_accessories']))
            $request_data['device_accessories'] = implode(',', $request_data['device_accessories']);

        $request_data['modify_day'] = thisDay();

        RepairOrder::findOrFail($sn)->update($request_data);

        if ($request->device_type) {

            $this->checkParameter('device_type', $request->device_type);
        }

        if ($request->device_brand) {

            $this->checkParameter('device_brand', $request->device_brand);
        }

        if ($request->device_model) {

            $this->checkParameter('device_model', $request->device_model);
        }

        if ($request->device_accessories) {

            foreach ($request->device_accessories as $device_accessory) {

                $this->checkParameter('device_accessory', $device_accessory);
            }
        }

        return redirect()->route('repair_order.edit', $sn);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $sn
     * @return \Illuminate\Http\Response
     */
    public function destroy($sn)
    {
        RepairOrder::findOrFail($sn)->delete();

        return redirect()->route('repair_order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param array $category
     * @return array
     */
    public function getParameter($categories)
    {
        $param = [];

        $parameters = Parameter::whereIn('category', $categories)->get();

        foreach ($parameters as $parameter) {

            $param[$parameter->category][] = $parameter->value;
        }

        return $param;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $category
     * @param string $value
     * @return void
     */
    public function checkParameter($category, $value)
    {

        Parameter::firstOrCreate(['category' => $category, 'value' => $value]);
    }

    public function export(Request $request)
    {
        $file_name = 'export_repair_order_' . date('YmdHis') . '.xls';

        return Excel::download(new RepairOrderExport($request), $file_name);
    }

    public function exportPreview(Request $request)
    {
        // $repair_orders = RepairOrder::all();
        $repair_orders = RepairOrder::withAccessors()->get(); // withAccessors 啟動字段替換

        $fields = array_intersect(Schema::getColumnListing((new RepairOrder)->getTable()), RepairOrder::getExportFields());

        $field_names = RepairOrder::getFieldNames();

        return view('excels.repair_order', compact('repair_orders', 'fields', 'field_names'));
    }

    public function generateOrderNo()
    {
        $latest_order = RepairOrder::orderByDesc('sn')->first();

        if (substr($latest_order->no, 0, 3) == 'LIY') {

        } else {
            $repair_order_no = 'LIY020918';
        }

        return $repair_order_no;
    }
}
