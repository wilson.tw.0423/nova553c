<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Show login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginPage()
    {
        return view('pages.login');
    }

    /**
     * Login.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        // 帳號：nova553c
        // 密碼：54996192

        $account_list = [
            'wilson' => [
                'role' => 'dev',
                'pwd' => '0110'
            ],
            'nova553c' => [
                'role' => 'admin',
                'pwd' => '54996192'
            ]
        ];

        if(!empty($request->username) && !empty($request->password)){

            if(!empty($account_list[$request->username]) && $request->password == $account_list[$request->username]['pwd']){

                // 建立 session in SessionRepository

                session(['username' => $request->username]);
                session(['user_role' => $account_list[$request->username]['role']]);

                return redirect('/dashboard');

            }else{
                
                return view('pages.login', ['err'=>"帳號或密碼錯誤"]);                
            }

        }else{

            return view('pages.login', ['err'=>"資料不完全"]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        // session()->forget('username');

        session()->flush();

        return redirect('/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
