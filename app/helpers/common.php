<?php


if (!function_exists('vd')) {
    function vd($data)
    {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
    }
}


if (!function_exists('thisDay')) {
    function thisDay()
    {
        return date('Y-m-d');
    }
}

if (!function_exists('toDate')) {
    function toDate($count, $period)
    {
        return date('Y-m-d', strtotime(strtotime($count.' '.$period, 'Y-m-d')));
    }
}

if (!function_exists('now')) {
    function now()
    {
        return date('Y-m-d H:i:s');
    }
}