<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repair_orders', function (Blueprint $table) {

            $table->id('sn');
            $table->string('no', 20)->unique();
            $table->string('name', 100)->nullable()->index();
            $table->string('mobile', 20)->nullable()->index();
            $table->string('phone', 20)->nullable()->index();
            $table->string('company_name', 255)->nullable()->index();
            $table->string('tax_id_number', 10)->nullable()->index();
            $table->date('order_day');
            $table->date('modify_day');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repair_orders');
    }
};
