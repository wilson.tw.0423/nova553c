<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_orders', function (Blueprint $table) {
            
            $table->integer('payment_pre_tax_price')->nullable()->change();
            $table->integer('payment_tax_price')->nullable()->change();
            $table->integer('payment_total_price')->nullable()->change();
            $table->integer('payment_downpayment')->nullable()->change();
            $table->integer('payment_finalpayment')->nullable()->change();
            $table->integer('payment_testing_charges')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
