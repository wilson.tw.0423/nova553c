<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_orders', function (Blueprint $table) {

            $table->after('tax_id_number', function ($table) {

                $table->string('engineer', 100)->nullable()->comment('收件工程師')->index();
                $table->string('device_type', 100)->nullable()->comment('設備類型')->index();
                $table->string('device_brand', 100)->nullable()->comment('品牌')->index();
                $table->string('device_model', 100)->nullable()->comment('型號')->index();
                $table->string('device_serial_number', 100)->nullable()->comment('序號')->index();
                $table->string('device_pwd_lock', 100)->nullable()->comment('密碼鎖');
                $table->string('device_pattern_lock', 100)->nullable()->comment('圖形鎖');


                $table->text('appearance_desc')->nullable()->comment('外觀檢測');
                $table->text('error_desc')->nullable()->comment('故障描述');
                $table->text('repair_report')->nullable()->comment('維修報告');

                $table->tinyInteger('data_delete_check')->default('0')->comment('確認可刪除資料');

                // $table->tinyInteger('before_check_btn')->default('0')->comment('維修前按鈕檢測');
                // $table->tinyInteger('after_check_btn')->default('0')->comment('維修後按鈕檢測');

                // $table->tinyInteger('before_check_monitor')->default('0')->comment('維修後螢幕檢測');
                // $table->tinyInteger('after_check_monitor')->default('0')->comment('維修後螢幕檢測');

                // $table->tinyInteger('before_check_monitor')->default('0')->comment('維修後螢幕檢測');
                // $table->tinyInteger('after_check_monitor')->default('0')->comment('維修後螢幕檢測');

                // $table->tinyInteger('before_check_monitor')->default('0')->comment('維修後螢幕檢測');
                // $table->tinyInteger('after_check_monitor')->default('0')->comment('維修後螢幕檢測');

                // $table->tinyInteger('before_check_monitor')->default('0')->comment('維修後螢幕檢測');
                // $table->tinyInteger('after_check_monitor')->default('0')->comment('維修後螢幕檢測');

                // $table->tinyInteger('before_check_monitor')->default('0')->comment('維修後螢幕檢測');
                // $table->tinyInteger('after_check_monitor')->default('0')->comment('維修後螢幕檢測');

                // $table->tinyInteger('before_check_monitor')->default('0')->comment('維修後螢幕檢測');
                // $table->tinyInteger('after_check_monitor')->default('0')->comment('維修後螢幕檢測');

                $table->integer('payment_pre_tax_price')->default('0')->comment('未稅金額');
                $table->integer('payment_tax_price')->default('0')->comment('稅金');
                $table->integer('payment_total_price')->default('0')->comment('總額');
                $table->integer('payment_downpayment')->default('0')->comment('已付訂金');
                $table->integer('payment_finalpayment')->default('0')->comment('應收尾款');
                $table->integer('payment_testing_charges')->default('0')->comment('檢測費用');

                $table->string('notify_method', 100)->nullable()->comment('通知方式');
                $table->date('notify_date')->nullable()->comment('通知日期');
                $table->date('pickup_date')->nullable()->comment('取件日期');
                $table->string('sales', 100)->nullable()->comment('收款業務');

            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
