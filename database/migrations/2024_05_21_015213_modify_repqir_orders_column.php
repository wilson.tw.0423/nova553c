<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('repair_orders', function (Blueprint $table) {

            $table->after('device_serial_number', function ($table) {

                $table->string('device_cpu', 100)->nullable()->comment('CPU');
                $table->string('device_ram', 100)->nullable()->comment('RAM');
                $table->string('device_hdd', 100)->nullable()->comment('HDD');
                $table->string('device_ssd', 100)->nullable()->comment('SSD');
                $table->string('device_power', 100)->nullable()->comment('Power');
                $table->text('device_accessories')->nullable()->comment('配件');
            });

            $table->after('repair_report', function ($table) {

                $table->tinyInteger('before_check_btn')->default('0')->comment('維修前按鈕檢測');
                $table->tinyInteger('after_check_btn')->default('0')->comment('維修後按鈕檢測');

                $table->tinyInteger('before_check_monitor')->default('0')->comment('維修前螢幕檢測');
                $table->tinyInteger('after_check_monitor')->default('0')->comment('維修後螢幕檢測');

                $table->tinyInteger('before_check_internet')->default('0')->comment('維修前網路檢測');
                $table->tinyInteger('after_check_internet')->default('0')->comment('維修後網路檢測');

                $table->tinyInteger('before_check_call')->default('0')->comment('維修前通話檢測');
                $table->tinyInteger('after_check_call')->default('0')->comment('維修後通話檢測');

                $table->tinyInteger('before_check_camera')->default('0')->comment('維修前鏡頭檢測');
                $table->tinyInteger('after_check_camera')->default('0')->comment('維修後鏡頭檢測');

                $table->tinyInteger('before_check_bottom')->default('0')->comment('維修前尾插檢測');
                $table->tinyInteger('after_check_bottom')->default('0')->comment('維修後尾插檢測');

                $table->tinyInteger('before_check_speaker')->default('0')->comment('維修前喇叭檢測');
                $table->tinyInteger('after_check_speaker')->default('0')->comment('維修後喇叭檢測');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
