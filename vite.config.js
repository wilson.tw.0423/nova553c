import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
                'resources/js/pages/repair_order/index.js',
                'resources/js/pages/repair_order/edit.js',
                'resources/js/pages/repair_order/create.js'
            ],
            refresh: true,
        }),
    ],
    resolve: {
        // alias: {
        //     '$': 'jQuery',
        // },
    },
});
