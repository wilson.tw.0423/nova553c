<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\RepairOrderController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\PdfController;
use Illuminate\Http\Request;
use App\Models\RepairOrder;
use App\Models\Parameter;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// test
Route::get('/test', [TestController::class, 'index'])->name('test');

Route::redirect('/', '/login');

Route::get('/login', [LoginController::class, 'showLoginPage'])->middleware('admin.user.auth')->name('login.page');

Route::post('/login', [LoginController::class, 'login'])->name('login');

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::redirect('/dashboard', '/repair_order');

// Route::get('/dashboard', function () {

//     return view('pages.dashboard');

// })->middleware('admin.user.auth')->name('dashboard');

// 任務
// Route::resource('task', TaskController::class)->middleware('admin.user.auth');

// 維修工單
Route::resource('repair_order', RepairOrderController::class)->middleware('admin.user.auth');

// 匯出

Route::post('/export/repair_order', [RepairOrderController::class, 'export'])->name('export.repair_order');

Route::get('/export_pre/repair_order', [RepairOrderController::class, 'exportPreview']);

// pdf

Route::get('/download-pdf/test', [TestController::class, 'domwloadPdf']);

Route::get('/download-pdf/{name}/{sn}', [PdfController::class, 'downloadPdf'])->name('download-pdf');

Route::get('/preview-pdf/{name}/{sn}', [PdfController::class, 'previewPdf'])->name('preview-pdf');








